---
date: 15-10-26
description: "10-26-15 Back from London"

featured_image: "/letters/1915/10-26-15 Back from London/page 1.jpg"

title: "10-26-15 Back from London"
---

{{< gallery dir="/letters/1915/10-26-15 Back from London" />}} {{< load-photoswipe >}}
