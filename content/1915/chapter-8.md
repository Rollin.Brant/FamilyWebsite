---
date: 15-11-17
description: "11-17-15 Preparing for Trenches plus photos"

featured_image: "/letters/1915/11-17-15 Preparing for Trenches plus photos/output.pdf"

title: "11-17-15 Preparing for Trenches plus photos"
---

{{< gallery dir="/letters/1915/11-17-15 Preparing for Trenches plus photos" />}} {{< load-photoswipe >}}
