---
date: 15-10-31
description: "10-31-15 Attached to Battalion 32"

featured_image: "/letters/1915/10-31-15 Attached to Battalion 32/page 1.jpg"

title: "10-31-15 Attached to Battalion 32"
---

{{< gallery dir="/letters/1915/10-31-15 Attached to Battalion 32" />}} {{< load-photoswipe >}}
