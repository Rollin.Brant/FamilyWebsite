---
date: 17-01-21
description: "1-21-17 Unfit for Active Service"

featured_image: "/letters/1917/1-21-17 Unfit for Active Service/Scan_20170418(0).jpg"

title: "1-21-17 Unfit for Active Service"
---

{{< gallery dir="/letters/1917/1-21-17 Unfit for Active Service" />}} {{< load-photoswipe >}}
