---
date: 16-03-06

description: "3-6-16 It will be nice to get back to a feather mattress"

featured_image: "/letters/1917/3-6-16 It will be nice to get back to a feather mattress/Scan_20170422(2).jpg"

title: "3-6-16 It will be nice to get back to a feather mattress"
---

{{< gallery dir="/letters/1917/3-6-16 It will be nice to get back to a feather mattress" />}} {{< load-photoswipe >}}
