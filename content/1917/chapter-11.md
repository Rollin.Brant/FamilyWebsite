---
date: 17-02-02

description: "2-2-17 Lieutenants get killed just the same as privates"

featured_image: "/letters/1917/2-2-17 Lieutenants get killed just the same as privates/Scan_20170421(0).jpg"

title: "2-2-17 Lieutenants get killed just the same as privates"
---

{{< gallery dir="/letters/1917/2-2-17 Lieutenants get killed just the same as privates" />}} {{< load-photoswipe >}}
