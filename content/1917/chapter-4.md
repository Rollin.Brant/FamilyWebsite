---
date: 9-11-11
description: "11-11-9 A Splendid Job"

featured_image: "/letters/1917/11-11-9 A Splendid Job/ScansImage-2.jpg"

title: "11-11-9 A Splendid Job"
---

{{< gallery dir="/letters/1917/11-11-9 A Splendid Job" />}} {{< load-photoswipe >}}
